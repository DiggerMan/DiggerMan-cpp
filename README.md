<h2><img src="https://emojis.slackmojis.com/emojis/images/1531849430/4246/blob-sunglasses.gif?1531849430" width="30"/> Hello, I Am Digger Man! <img src="https://media.giphy.com/media/12oufCB0MyZ1Go/giphy.gif" width="50"></h2>
<img align='right' src="https://media.giphy.com/media/M9gbBd9nbDrOTu1Mqx/giphy.gif" width="230">
<p><em>Software Engineer on C++
</a><img src="https://media.giphy.com/media/WUlplcMpOCEmTGBtBW/giphy.gif" width="30"> 
</em></p>


![GitHub followers](https://img.shields.io/github/followers/diggerman-cpp?label=Follow&style=social)
[![website](https://img.shields.io/badge/Website-46a2f1.svg?&style=flat-square&logo=Google-Chrome&logoColor=white&link=https://diggerman-cpp.github.io/)](https://diggerman-cpp.github.io/)


```cpp
isEmpty(const std::string& str);
```
**👆 This function can be found at [Digger String](https://github.com/DiggerMan-cpp/DiggerString)**

### <img src="https://media.giphy.com/media/VgCDAzcKvsR6OM0uWg/giphy.gif" width="50"> A little more about me...  

```cpp
class DiggerMan
{
private:
   const char* telegram = "https://t.me/kernel86dll/";
   const char* blasthack = "https://www.blast.hk/members/343182/";
   const char* github = "https://github.com/DiggerMan-cpp/";
public:
    const char* NickName = "Digger Man";  
    std::vector<const char*> Code = {"C++", "C", "ASM"};
    std::vector<const char*> AskMeAbout = {"Linux", "Reverse-Engineering", "Desktop Development"};
};
```

<img src="https://media.giphy.com/media/LnQjpWaON8nhr21vNW/giphy.gif" width="60"> <em><b>I love connecting with different people</b> so if you want to say <b>hi, I'll be happy to meet you more!</b> 😊</em>

---
<!--START_SECTION:waka-->

**🐱 My GitHub Data** 
> 📜 13 Public Repositories 
 > 
> 🔑 11 Private Repositories 
 > 


```text
🌞 Morning                151 commits         ███░░░░░░░░░░░░░░░░░░░░░░   12.56 % 
🌆 Daytime                178 commits         ██████████░░░░░░░░░░░░░░░   38.05 % 
🌃 Evening                211 commits         ████████░░░░░░░░░░░░░░░░░   31.81 % 
🌙 Night                  185 commits         ████░░░░░░░░░░░░░░░░░░░░░   17.59 % 
```


📊 **This Week I Spent My Time On** 

```text
🕑︎ Time Zone: Russia/Moscow

💬 Programming Languages: 
 C++ - 12h 47m (86.30%)
 IDA - 28m (4.38%)
 HTML - 27m (3.88%)
 CMake - 16m (2.46%)
 JSON - 5m (1.06%)
 Python - 3m (0.39%)
 Markdown - 3m (0.31%)
 Other - 0m (0.16%)
 JavaScript - 0m (0.03%)

🔥 Editors: 
QT Creator                 19 hrs 59 mins      ████████████████████░░░░░   79.74 % 
Visual Studio              7 hrs 42 mins       █████░░░░░░░░░░░░░░░░░░░░   20.26 % 
```

**I Mostly Code in C++** 

```text
C++                      8 repos                 
C                        4 repos            
GO                       2 repos                 
```

<!--END_SECTION:waka-->
